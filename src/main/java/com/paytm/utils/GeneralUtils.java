package com.paytm.utils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.testng.reporters.Files;

public class GeneralUtils {
	
	public static String readFileContent(String filePath) throws IOException {
		return Files.readFile(new File(filePath));
	}

	
	public static int randomNumberGenerator(int min, int max) {
		Random randomGeneratorEngine = new Random();
		return randomGeneratorEngine.nextInt(max+1-min)+min;
	}
	
	public static double randomNumberGenerator(double min, double max) {
		double val = min+Math.random()*(max-min);
		val = val*100;
		val = Math.round(val);
		val = val/100;
		return val;
	}
	
	public static File createNewFileWithTimeStamp(String directoryPath, String fileName, String extension) {
		File directory = new File(directoryPath);
		if(!directory.exists())
			directory.mkdirs();
		if(extension.startsWith(".")){
			extension=extension.substring(1);
		}
		return new File(directory+"/"+fileName+"_"+System.currentTimeMillis()+"."+extension);
	}
	
	@SuppressWarnings("unchecked")
	public static <O extends Comparable> boolean isEveryValueInListInRange(List<O> list, O minVal, O maxVal){
		boolean areAllValuesInRange = true;
		for(O current : list) {
			if(current.compareTo(minVal)>=0 && current.compareTo(maxVal)<=0) {
				continue;
			}
			else {
				areAllValuesInRange = false;
				break;
			}
		}
		return areAllValuesInRange;
	}
	
	public static void main(String[] args) {
		List<Double> list = Arrays.asList(65399.0, 43500.0, 99500.0, 98500.0, 41000.0);
		System.out.println(isEveryValueInListInRange(list, 40936.65945464068, 133640.260574349));
	}
	
}
