package com.paytm.dataproviders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.DataProvider;

public class GridDataProvider {
	
	private static Properties dataPropertyFile ;
	static {
		dataPropertyFile = new Properties();
		try {
			dataPropertyFile.load(new FileInputStream(new File("./src/test/resources/data.properties")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@DataProvider(name="GridDataProvider")
	public static Object[][] getDataProvider(){
		Object[][] dataToReturn;
		String[] gridArray = dataPropertyFile.getProperty("grid").split(",");
		dataToReturn = new Object[gridArray.length][1];
		for(int i=0; i<gridArray.length; i++) {
			dataToReturn[i][0] = gridArray[i];
		}
		return dataToReturn;
	}
	
	public static void main(String[] args) {
		Object[][] data = getDataProvider();
		for(int i=0; i<data.length; i++) {
			for(int j=0; j<data[i].length; j++) {
				System.out.println(data[i][j]);
			}
			System.out.println("\n");
		}
	}
}
