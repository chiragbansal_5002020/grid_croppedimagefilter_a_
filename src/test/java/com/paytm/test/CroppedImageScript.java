package com.paytm.test;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.paytm.utils.Xls_Writer;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class CroppedImageScript {
	
	private static final int MAX_PAGE_SIZE = 30;
	FileDumper dataDumper;
	//Xls_Writer excelWriter;
	
	@BeforeTest
	public void setup() {
		RestAssured.baseURI="https://catalog.paytm.com";	
	}
	
	@Test(dataProvider="GridDataProvider",
			dataProviderClass=com.paytm.dataproviders.GridDataProvider.class)
	public void testCroppedImages(String categoryID) throws IOException {
		
//		excelWriter = new Xls_Writer("./src/test/resources/Result",
//				"test-glpid-"+categoryID+".xlsx",
//				"Category-"+categoryID,
//				Arrays.asList("Images Not Cropped"),
//				false);
		
		dataDumper = new FileDumper("./src/test/resources/Result", "Category-"+categoryID+".txt");
		String gridURL="/test-glpid-"+categoryID;
		boolean isTestPassed=true;
		
		int count_uncroppedImages = 0;
		int count_total=0;
		
		Response gridResponse = given()
			.when()
			.get(gridURL)
			.then()
			.extract()
			.response();
		
		categoryID = Integer.toString((Integer)gridResponse.path("id"));
		System.out.println("Category ID after extraction -- "+categoryID);
		gridURL = "test-glpid-"+categoryID;
		
		int totalProducts = gridResponse.path("totalCount");
		int numberOfPages = totalProducts/MAX_PAGE_SIZE+1;
		
		System.out.print("Running tool for category ID : "+categoryID);
		System.out.println("Total Products : "+totalProducts);
		System.out.println("Total Pages : "+numberOfPages);
		
		int threadsCounter=0;
		List<Engine> Engine2Threads = new ArrayList<>();
		
		for(int pageCount=1; pageCount<=numberOfPages; pageCount++) {
			
			Engine e = new Engine(gridURL, pageCount);
			Engine2Threads.add(e);
			e.start();
			threadsCounter++;
			
			if(threadsCounter==5) {
				for(Engine eng : Engine2Threads) {
					try {
						eng.join();
						if(eng.getListOfUncroppedUrls().size()>0)
							isTestPassed=false;
						//excelWriter.dumpListInExcel(eng.getListOfUncroppedUrls());
						dataDumper.writeListToFile(eng.getListOfUncroppedUrls());
						count_uncroppedImages+=eng.getListOfUncroppedUrls().size();
						count_total+=eng.getImageCount();
						eng.stop();
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
				Engine2Threads = null;
				Engine2Threads = new ArrayList<>();
				
				threadsCounter=0;			
			}
			
		}
		
		for(Engine eng : Engine2Threads) {
			try {
				eng.join();
				if(eng.getListOfUncroppedUrls().size()>0)
					isTestPassed=false;
				//excelWriter.dumpListInExcel(eng.getListOfUncroppedUrls());
				dataDumper.writeListToFile(eng.getListOfUncroppedUrls());
				count_uncroppedImages+=eng.getListOfUncroppedUrls().size();
				count_total+=eng.getImageCount();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
		
	//  excelWriter.finishExcelWriting();
		
	 dataDumper.writeToFile("\n\n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	 dataDumper.writeToFile("Total Count  : "+count_total);
	 dataDumper.writeToFile("Failed Count : "+count_uncroppedImages);
	 dataDumper.closeFile();
	Assert.assertTrue(isTestPassed,
			"List size of un cropped images should be 0");
		
	}
}
