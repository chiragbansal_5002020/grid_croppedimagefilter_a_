package com.paytm.test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.paytm.utils.Timer;

public class FileDumper {
	File file ;
	FileWriter writer;

	public FileDumper(String dirPath, String fileName) {
		new File(dirPath).mkdirs();
		file = new File(dirPath+"/"+fileName);
		try {
			writer = new FileWriter(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void writeToFile(String line) {
		try {
			synchronized  (this){
				
				writer.write(line);
				writer.write("\n");
				writer.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void writeListToFile(List<String> list) {
		if(list.size()>0) {
			for(String line : list) {
				writeToFile(line);
			}
		}
	}
	
	private File getNewFile(String filePath) {
		File file = new File(filePath);
		if(file.exists() && !file.isDirectory()) {
			String extension = filePath.substring(filePath.lastIndexOf(".")+1);
			filePath = filePath.substring(0, filePath.lastIndexOf("."+extension));
			filePath+="_"+Timer.getCurrentTimeStamp()+"."+extension;
			file = new File(filePath);
		}
		return file;
	}

	public void closeFile() {
		if(writer!=null) {
			try {
				writer.flush();
				writer.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

}
