package com.paytm.test;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import io.restassured.response.Response;

public class Engine extends Thread{

	private List<String> notCroppedList;
	private String gridURL;
	private int pageCount;
	private int totalImagesOnGrid;
	
	
	public Engine(String gridURL, int pageCount) {
		this.gridURL = gridURL;
		this.notCroppedList=new ArrayList<>();
		this.pageCount=pageCount;
	}

	@Override
	public void run() {
		System.out.println("Checking product URLs on page number :  "+pageCount);
	     Response response=	given()
		.queryParam("page_count", pageCount)
		.when()
		.get(gridURL)
		.then()
		.extract().response();
	     List<String> imageUrls = response.path("grid_layout.image_url");
	     List<Integer> productIds = response.path("grid_layout.product_id");
	    
	    System.out.println("Image URLs -- "+imageUrls);
	    
	    totalImagesOnGrid = imageUrls.size();
		
			for(int i=0; i< imageUrls.size(); i++) {
			if(!isUrlContainsCroppedImage(imageUrls.get(i)))
				notCroppedList.add(imageUrls.get(i) + "   |  "+productIds.get(i));
			}
		
	    
	}

	private boolean isUrlContainsCroppedImage(String url) {
		String urlPart = url.substring(url.lastIndexOf('/')+1,url.lastIndexOf('.')-1);
		if(urlPart.startsWith("a_"))
			return true;
		else
			return false;
	}
	
	public List<String> getListOfUncroppedUrls(){
		return notCroppedList;
	}
	
	public int getImageCount() {
		return totalImagesOnGrid;
	}
	
}
