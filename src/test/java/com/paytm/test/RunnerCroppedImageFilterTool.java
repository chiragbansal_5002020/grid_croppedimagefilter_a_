package com.paytm.test;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

public class RunnerCroppedImageFilterTool {
	public static void main(String[] args) {
		TestNG myTestNG = new TestNG();   
	     
	  
	      XmlSuite mySuite = new XmlSuite(); 
	      mySuite.setName("Suite_ImageFilter"); 
	      mySuite.setParallel(XmlSuite.ParallelMode.NONE);   
 
	     XmlTest myTest = new XmlTest(mySuite); 
	     myTest.setName("GridImageURLFilterWith__a_");   
	  
	     List<XmlClass> myClasses = new ArrayList<XmlClass>();
	     myClasses.add(new XmlClass(com.paytm.test.CroppedImageScript.class));   

	     myTest.setXmlClasses(myClasses);   
	     
	     List<XmlTest> myTests = new ArrayList<XmlTest>(); 
	     myTests.add(myTest);   
 
	     mySuite.setTests(myTests);   

	     List<XmlSuite> mySuites = new ArrayList<XmlSuite>(); 
	     mySuites.add(mySuite);   
	     
	     myTestNG.setXmlSuites(mySuites);
	     mySuite.setFileName("./testng.xml"); 
	     mySuite.setThreadCount(1);   
	     myTestNG.run();
		}
}
